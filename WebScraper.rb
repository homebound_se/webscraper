#
# Web scraping framework for Ruby
#

require 'csv'

require_relative 'DataSync'
require_relative 'PageFetcher'
require_relative 'PageParser'

# This class manages all the web scraping resources and glues them all
# together
class WebScraper
  def initialize(sync: DataSync.new,
                 parser: PageParser.new,
                 fetcher: PageFetcher.new)
    @sync = sync
    @parser = parser
    @fetcher = fetcher
  end

  # TODO: When running a scraper, we should perform some kind of proxy
  # switching.
  def run_scraper(params)
    total = 0
    puts "Executing web scraper..."

    @sync.init(@fetcher.save_path)

    # Fetch intermediate files for scraping.
    @fetcher.fetch(params) do |file|
      html = ""
      html = File.read(file) if File.exist? file

      @parser.parse(filename: file, html: html) do |f|
        @sync.save_data(fields: f)
      end

      total = total + 1;
    end

    puts "Total pages: #{total}"
  end
end

# End of file
