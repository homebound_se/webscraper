#
# Web scraping framework for Ruby
#

require 'csv'
require 'mechanize'
require 'nokogiri'

class PageFetcher
  attr_accessor :name, :data_dir, :auth_required, :url, :save_path, :proxies

  PROXY_BASE = 'http://proxylist.hidemyass.com'
  PROXY_FILE = "_proxy.csv"

  def initialize(data_dir: "data", name: "scraper", url: "http://www.google.com")
    @url = url
    @name = name
    @data_dir = data_dir
    @save_path = "#{@data_dir}/#{@name}"
    @cycle_proxy = 0
    @auth_required = false

    FileUtils.mkdir_p(@save_path) unless File.exists?(@save_path)

    @agent = Mechanize.new do |a|
      a.user_agent_alias = 'Mac Safari'
    end

    load_proxies
  end

  def random_proxy
    proxy = @proxies.sample
    puts "Using Proxy: #{proxy[0]}:#{proxy[1]} from #{proxy[2]}"
    @agent.set_proxy(proxy[0], proxy[1])
    self
  end

  def auth_session(username: "username", password: "password")
  end

  def logout
  end

  def fetch_page(url, fname)
    file = "#{@save_path}/#{fname}"

    unless File.exist? file
      loop do
        begin
          puts "Fetching: #{url}"
          @agent.get(url)
        rescue Exception => e
          puts "Error: #{e}"
          self.random_proxy
          next
        else
          @agent.page.save(file)

          @cycle_proxy = @cycle_proxy + 1
          if @cycle_proxy > 100
            @cycle_proxy = 0
            self.random_proxy
          end

          break
        ensure
          sleep 2 + rand
        end
      end # loop do
    end

    file
  end

  # The #fetch function should yield to the final page where the data
  # is to be scraped from. The page should be saved to an intermediate
  # directory. This directory is defined in the @save_path member
  # variable.
  def fetch(params: {})
    puts "Fetching data: #{@url} to #{@save_path}/file.html}"
    yield "#{@save_path}/file.html"
  end

  # Load the proxy list from the _proxy.csv file.
  #
  # @param reload This will make the method download the new list of proxies
  #               from the proxy source (hidemyass).
  #
  # @return self
  def load_proxies(reload: false)
    csv = nil
    url = "#{PROXY_BASE}/1"
    page = 1
    proxy = []
    do_reload = (reload or (not File.exist? PROXY_FILE))

    # TODO: Remove the intermediate cache files
    # delete_files if reload

    # Open an instance of the proxy csv file for writing. This will happen
    # if the file don't exist or a reload was called.
    csv = CSV.open(PROXY_FILE, "wb") if do_reload

    loop do
      fname = fetch_page(url, "proxy_base_p#{page}.html")
      doc = Nokogiri::HTML(File.open(fname))

      doc.xpath("//table[@id='listable']/tbody/tr").each do |r|
        proxy.clear

        # Fields
        # Last Update, IP Address, Port, Country, Speed, Connection Time, Type, Anon

        c = r.xpath("td")

        # Defeat IP Address obfuscation mechanism
        hidden = c[1].xpath('span/style').text
                                         .strip.gsub(/\./,'').split("\n")
        hidden.map! do |e|
          e = e.sub(/\{.*/,'') if e.include? '{display:none}'
          e = nil if e.include? '{display:inline}'
          e
        end

        hidden.compact!

        ip = []
        c[1].xpath('span').children.each do |e|
          next if e.name.include? 'style'
          next if (not e['style'].nil?) and e['style'].include? 'display:none'
          next if e.text.empty?
          next if e.text.include? "." and e.text.length == 1

          show = true
          hidden.each do |h|
            show = false if h.eql? e['class']
          end

          ip << e.text.sub(/\./,'') if show
        end
        proxy << ip.join('.')
        # IP Address End

        proxy << c[2].text.strip.to_i # Port
        proxy << c[3].text.strip      # Country
        speed = c[4].xpath('div')[0]['value'].to_i # Speed
        proxy << speed
        ctime = c[5].xpath('div')[0]['value'].to_i # Connection Time
        proxy << ctime
        proxy << c[6].text.strip      # Type
        anon = c[7].text.strip
        proxy << anon

        next if ip.length != 4
        next if speed >= 7000
        next if ctime >= 3000

        next if anon.include? 'Low' or
                anon.include? 'None' or
                anon.include? 'Medium'

        csv << proxy
        # break is DEBUG
      end # doc.xpath

      break if doc.xpath("//a[@class='next']").length == 0

      url = "#{PROXY_BASE}#{doc.xpath("//a[@class='next']")[0]['href']}"
      page = page + 1

      # break if DEBUG
    end if do_reload # loop

    csv.close if not csv.nil?

    # Load the list from the specified proxy csv file
    @proxies = CSV.read(PROXY_FILE)

    self
  end # load_proxies
end

# End of file
