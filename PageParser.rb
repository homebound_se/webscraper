#
# Web scraping framework for Ruby
#

require 'mechanize'
require 'nokogiri'

#
# A page parser will parse the required information from the website. This
# is the final step of the web data fetching process.
#

class PageParser
  attr_reader :fields

  def initialize
    @fields = Hash.new
  end

  def parse(filename: String.new, html: String.new)
    yield @fields
  end

end

# End of file
